// trie.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <string>
#include <iostream>
#include <vector>
#include <map>

using std::map;
using std::vector;
using std::string;

typedef map<char, int> edges;
typedef map<char, int>::iterator miter;
typedef vector<edges> trie;

trie build_trie(vector<string> & patterns) {
	trie t;
	// write your code here
	int nop = patterns.size(); // no of patterns

	string str;
	int iNodeNum = 1;

	for (int i = 0; i < nop; i++) // foreach pattern
	{	
		int iCurrNode = 0;

		for (int j = 0; j < patterns[i].length(); j++)
		{
			char edgeVal = patterns[i][j];
			
			
			//traverse the tree
			if (t.size() > 0) // no elements in the vector
			{
				if (iCurrNode < t.size())
				{
					map<char, int>::iterator iter = t[iCurrNode].find(edgeVal);
					
					if (iter != t[iCurrNode].end()) // current char matched
						iCurrNode = iter->second;
					else
					{
						edges edgeMap;
						t[iCurrNode].insert(std::pair<char, int>(edgeVal, iNodeNum));
						t.push_back(edgeMap);//insert a child also insert in in vector
						iCurrNode = iNodeNum;
						iNodeNum++;
					}
				}
				else
				{
					edges edgeMap;

					edgeMap.insert(std::pair<char, int>(edgeVal, iNodeNum));
					t.push_back(edgeMap);
					iCurrNode = iNodeNum;
					iNodeNum++;
				}
			}
			else 
			{
				edges edgeMap;

				edgeMap.insert(std::pair<char, int>(edgeVal, iNodeNum));
				t.push_back(edgeMap);
				iCurrNode = iNodeNum;
				iNodeNum++;
			}
		}

	}
	return t;
}

void traverse_all(trie &t, int nodeNum, string pattern )
{
	if (t.size() > nodeNum)
	{
		map<char, int>::iterator iter;
		for ( iter = t[nodeNum].begin(); iter != t[nodeNum].end(); iter++)
		{
			pattern = pattern + iter->first;
			traverse_all(t, iter->second, pattern);
			std::cout << pattern << "\n";
		}
		
	}

	return;

}

int main() {
	size_t n;
	//std::cin >> n;
	vector<string> patterns;
	/*for (size_t i = 0; i < n; i++) {
		string s;
		std::cin >> s;
		patterns.push_back(s);
	}*/

	patterns.push_back("ATAGA");
	patterns.push_back("ATC");
	patterns.push_back("GAT");

	trie t = build_trie(patterns);
	for (size_t i = 0; i < t.size(); ++i) {
		for (const auto & j : t[i]) {
			std::cout << i << "->" << j.second << ":" << j.first << "\n";
		}
	}

	string pattern = "";
	traverse_all(t, 0, pattern);
	return 0;
}